#!/usr/bin/bash

## This is NOT the application.
## A python app is coming soon. This was just the start of a simple shell script that I quickly decided was TOO simple.


#start jack
#use midi=seq for alsa and jack crossover ports
jackd --realtime --driver alsa --midi seq &

# start the synths 
grep -i name= ./jack/*.aj | cut -f2 -d'"' | \
    sed '/track\|Out\|left\|right\|[Ss]ystem\|capture\|[Mm]idi\|LASH/d'

# start the effect racks

# start the sequencer

# restore the jack session

# start a patchbay
patchage

