<?xml version="1.0" encoding="utf-8"?>
<alsa>
  <client name="System">
    <port id="0" />
    <port id="1" />
  </client>
  <client name="Midi Through">
    <port id="0" />
  </client>
  <client name="LPK25">
    <port id="0">
      <connection client="Midi Through" port="0" />
    </port>
  </client>
  <client name="jack_midi" />
  <client name="Patchage" />
  <client name="LASH Server" />
</alsa>
<jack>
  <client name="system">
    <port name="capture_1" />
    <port name="capture_2" />
    <port name="midi_capture_1">
      <connection port="MusE:LPK25:midi/playback_1_in" />
    </port>
  </client>
  <client name="yoshimi">
    <port name="left">
      <connection port="system:playback_1" />
    </port>
    <port name="right">
      <connection port="system:playback_2" />
    </port>
    <port name="track_1_l" />
    <port name="track_1_r" />
    <port name="track_2_l" />
    <port name="track_2_r" />
    <port name="track_3_l" />
    <port name="track_3_r" />
  </client>
  <client name="system">
    <port name="midi_capture_4" />
  </client>
  <client name="yoshimi">
    <port name="track_4_l" />
    <port name="track_4_r" />
    <port name="track_5_l" />
    <port name="track_5_r" />
    <port name="track_6_l" />
    <port name="track_6_r" />
    <port name="track_7_l" />
    <port name="track_7_r" />
    <port name="track_8_l" />
    <port name="track_8_r" />
    <port name="track_9_l" />
    <port name="track_9_r" />
    <port name="track_10_l" />
    <port name="track_10_r" />
    <port name="track_11_l" />
    <port name="track_11_r" />
    <port name="track_12_l" />
    <port name="track_12_r" />
    <port name="track_13_l" />
    <port name="track_13_r" />
    <port name="track_14_l" />
    <port name="track_14_r" />
    <port name="track_15_l" />
    <port name="track_15_r" />
    <port name="track_16_l" />
    <port name="track_16_r" />
  </client>
  <client name="MusE">
    <port name="LPK25:midi/capture_1_out" />
    <port name="Midi-Through:midi/capture_1_out">
      <connection port="yoshimi:midi in" />
      <connection port="yoshimi-1:midi in" />
    </port>
    <port name="yoshimi:midi in_out" />
    <port name="Out 1-0">
      <connection port="system:playback_1" />
    </port>
    <port name="Out 1-1">
      <connection port="system:playback_2" />
    </port>
  </client>
  <client name="Calf Studio Gear">
    <port name="reverb Out #1">
      <connection port="system:playback_1" />
    </port>
    <port name="reverb Out #2">
      <connection port="system:playback_2" />
    </port>
  </client>
  <client name="yoshimi-1">
    <port name="left">
      <connection port="system:playback_1" />
    </port>
    <port name="right">
      <connection port="system:playback_2" />
    </port>
    <port name="track_1_l" />
    <port name="track_1_r" />
    <port name="track_2_l" />
    <port name="track_2_r" />
    <port name="track_3_l" />
    <port name="track_3_r" />
    <port name="track_4_l" />
    <port name="track_4_r" />
    <port name="track_5_l" />
    <port name="track_5_r" />
    <port name="track_6_l" />
    <port name="track_6_r" />
    <port name="track_7_l" />
    <port name="track_7_r" />
    <port name="track_8_l" />
    <port name="track_8_r" />
    <port name="track_9_l" />
    <port name="track_9_r" />
    <port name="track_10_l" />
    <port name="track_10_r" />
    <port name="track_11_l" />
    <port name="track_11_r" />
    <port name="track_12_l" />
    <port name="track_12_r" />
    <port name="track_13_l" />
    <port name="track_13_r" />
    <port name="track_14_l" />
    <port name="track_14_r" />
    <port name="track_15_l" />
    <port name="track_15_r" />
    <port name="track_16_l" />
    <port name="track_16_r" />
  </client>
</jack>
